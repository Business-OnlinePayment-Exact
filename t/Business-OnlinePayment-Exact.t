# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Business-OnlinePayment-Exact.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More;
use Term::ReadLine;

BEGIN {
  unless ( $ENV{RELEASE_TESTING} ) {
    my $msg = 'Author test.  Set $ENV{RELEASE_TESTING} to a true value to run.';
    plan( skip_all => $msg );
    done_testing();
    exit(0);
  }
  use_ok('Business::OnlinePayment::Exact');
};

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

my $term = new Term::ReadLine 'E-Xact Test';
diag("Please enter a test account for E-Xact");
my $prompt = "ExactID: ";
my $login = $term->readline($prompt);
diag("Please enter the password for the test account $login");
$prompt = "Password: ";
my $pass = $term->readline($prompt);
diag("Please enter a valid credit card to test (it will not be charged)");
$prompt = "Card Number: ";
my $card = $term->readline($prompt);
diag("Please enter an expiry date for the card in the form MMYY");
$prompt = "Expiry: ";
my $expiry = $term->readline($prompt);
diag("Please enter a name to match the card");
$prompt = "Name: ";
my $name = $term->readline($prompt);

my $tx;
ok($tx = new Business::OnlinePayment('Exact'), 'New Exact');
ok($tx->content(
    amount => '0.01',
    card_number => $card,
    expiration => $expiry,
    name => $name,
    action => 'normal authorization',
    login => $login,
    password => $pass,
    referer => 'Business::OnlinePayment::Exact testing',
    ),
    'Add Some Content');

my $return = eval { $tx->test_transaction(1) };

my $error = $@;

ok($error, 'enable test mode dies');

ok($tx->submit(), 'submit');

ok($tx->is_success(), 'Success!!!')
  or diag "Card was rejected: ", $tx->error_message();

my $auth;
ok($auth = $tx->authorization(), "authorization $auth");
my $err;
ok($err = $tx->error_message(), "error $err");
my $on;
ok($on = $tx->order_number(), "order number $on");


done_testing();
